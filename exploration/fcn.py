import numpy as np
import math, random, string
import os, sys
import torch
import torch.nn as nn
import torch.multiprocessing
from torch.utils.data import Dataset
from model.UNet import Model

import glob
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

path = "/home/hackathon/dem/hackII"
img_paths = [file for file in glob.glob(path + "/all_frames_5m6b/*.npy")]
img_paths.sort()
label_paths = [file for file in glob.glob(path + "/all_masks_5m6b/*.npy")]
label_paths.sort()
images = np.asarray([np.load(pth, allow_pickle=True) for pth in img_paths])#.transpose([0,3,1,2])
labels = np.asarray([np.load(pth, allow_pickle=True) for pth in label_paths]).astype(np.double)

model = Model(1)
model = model.cuda()
model = nn.DataParallel(model, device_ids=[0, 1, 2, 3])

model.train()
y = model(torch.Tensor(images[0:2, :, :, :]))

batch_size = 16
optimizer = torch.optim.Adam(model.parameters(), lr=1.0e-4)
criterion = nn.MSELoss()

training_ratio = 0.9

train_images=images[0:int(images.shape[0]*training_ratio), :, :,:]
train_labels=labels[0:train_images.shape[0], :, :]

eval_images=images[train_images.shape[0]:-1, :, :, :]
eval_labels=labels[train_images.shape[0]:-1, :, :]

print('{} images for training, {} images for validation'.format(train_labels.shape[0],eval_labels.shape[0]))


def shuffle_in_unison_scary(a, b):
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)


shuffle_in_unison_scary(images,labels)

train_dataset = torch.utils.data.TensorDataset(torch.tensor(train_images),torch.tensor(train_labels)) 
train_loader = torch.utils.data.DataLoader(train_dataset,batch_size=batch_size)

eval_dataset = torch.utils.data.TensorDataset(torch.tensor(eval_images),torch.tensor(eval_labels)) 
eval_loader = torch.utils.data.DataLoader(eval_dataset,batch_size=batch_size*10)

for i in range(30):
    e = -1
    for x, label in train_loader:
        e += 1
        model.train()
        optimizer.zero_grad()
        pred = model(x.cuda()).squeeze()

        pred = pred.double()
    
        loss = criterion(pred.cpu(), label)
        loss.backward()
        optimizer.step()
        
        if e % 16 == 0:
            print('Step: {}, Loss: {}'.format(e, loss*100))
        
        if e % 32 == 0:
            with torch.no_grad():
                l = 0.
                num = 0.
                for x, label in eval_loader:
                    model.eval()
                    pred = model(x.cuda()).squeeze()
                    pred = pred.double()
                    l += criterion(pred.cpu(), label)
                    num += 1
                print('Validation Loss: {}'.format(l / num * 100.))


def checkpoint(model,directory):
    torch.save(model.state_dict(), directory+'.pth')
    torch.save(optimizer.state_dict(), directory+'_optimizer.opt')


checkpoint(model,"./checkpoint")

