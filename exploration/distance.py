# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 13:14:31 2019
Here we want to propose a metrics that can measure the similarity of 
predicted vs real label data in soil erosion detection project. 
we measure the morphological difference between two binary matrices
The algorithm find closest point iterative, steps are:
1. For each point in the predicted data, match the closest point in the real data;
2. Measure the distance between these two points; (Manhattan distance)
3. Iterate (through all the points in the predicted dataset).
4. If there are unmatched points in the real data, for each points, match the closest point
   in the predicted data;
5. Measure the distance between two points;
6. Iterate through all the unmatched points.
@author: shirui
"""

import numpy as np
import torch.nn as nn
import torch
import math

def match_point(point, real_idx): ## Brute force
    """
    return Distance of nearest cell having 1 in a binary matrix
    """
    x_, y_ = point
    mymindist = 1e5
    for i in range(len(real_idx[0])):
        dist = abs(real_idx[0][i]-x_)+abs(real_idx[1][i]-y_) #Manhattan distance 
        if dist<mymindist:
            mymindist = dist
            matched_point = (real_idx[0][i], real_idx[1][i])
        
    return mymindist, matched_point
         
def distance_measure(real, pred):
    """ 
    real and pred are 2D numpy arrays with same dimension
    Measure the morphological difference between two binary matrices
    """

    # print(np.sum(real))
    # print(np.sum(pred))
    real_idx = np.nonzero(real)
    pred_idx = np.nonzero(pred)
    distSum = 0
    matched_point_list = set()
    
    if (len(real_idx[0])==0):
        if (len(pred_idx[0])==0):
            return 0
        else: 
            distSum = len(pred_idx[0])
            return distSum
          
    for i in range(len(pred_idx[0])):
        point = (pred_idx[0][i], pred_idx[1][i])
        dist, matched_point = match_point(point, real_idx)
        distSum += dist
        matched_point_list.add(matched_point)

    real_idx_converted = set()  
    for i in range(len(real_idx[0])):
        real_idx_converted.add((real_idx[0][i],real_idx[1][i]))
        
    if real_idx_converted.difference(matched_point_list) is not None:
        for points in (real_idx_converted.difference(matched_point_list)):
            dist, matched_point = match_point(points, pred_idx)
            distSum += dist
    
    """
    plt.imshow(real)
    plt.show()
    plt.imshow(pred)
    plt.show()
    print('distance between two matrices: %d' %distSum)        
    """        
    return distSum       
        
# Will error if any one of them is empty!!
def distance_measure_batch_torch(real, pred):
    dist=0.
    real=real.cpu().numpy()
    pred=pred.cpu().numpy()
    for i in range(real.shape[0]):
        # print(real[i].shape)
        # print(pred[i].shape)
        real[real<0.2]=0
        dist+=distance_measure(real[i],pred[i])
    dist/=real.shape[0]
    return dist

 # From https://discuss.pytorch.org/t/is-there-anyway-to-do-gaussian-filtering-for-an-image-3d-3d-in-pytorch/12351/3
def get_gaussian_kernel(kernel_size=3, sigma=6, channels=3):
    # Create a x, y coordinate grid of shape (kernel_size, kernel_size, 2)
    x_coord = torch.arange(kernel_size)
    x_grid = x_coord.repeat(kernel_size).view(kernel_size, kernel_size)
    y_grid = x_grid.t()
    xy_grid = torch.stack([x_grid, y_grid], dim=-1).float()

    mean = (kernel_size - 1)/2.
    variance = sigma**2.

    # Calculate the 2-dimensional gaussian kernel which is
    # the product of two gaussian distributions for two different
    # variables (in this case called x and y)
    gaussian_kernel = (1./(2.*math.pi*variance)) *\
                      torch.exp(
                          -torch.sum((xy_grid - mean)**2., dim=-1) /\
                          (2*variance)
                      )

    # Make sure sum of values in gaussian kernel equals 1.
    gaussian_kernel = gaussian_kernel / torch.sum(gaussian_kernel)

    # Reshape to 2d depthwise convolutional weight
    gaussian_kernel = gaussian_kernel.view(1, 1, kernel_size, kernel_size)
    gaussian_kernel = gaussian_kernel.repeat(channels, 1, 1, 1)

    gaussian_filter = nn.Conv2d(in_channels=channels, out_channels=channels,
                                kernel_size=kernel_size, groups=channels, bias=False,padding=5,stride=1)

    gaussian_filter.weight.data = gaussian_kernel
    gaussian_filter.weight.requires_grad = False
    
    return gaussian_filter

guassian=None
sig=nn.Sigmoid()
with torch.no_grad():
    guassian = get_gaussian_kernel(11,11,channels=1)
    guassian=guassian.double()

def metric(output, target):
    loss=(torch.sum(output)-torch.sum(target))**2
    with torch.no_grad():
      target=guassian.forward(target)  
    loss+=torch.sum((output-0.5)**2)
    print(output.shape)
    print(target.shape)
    loss-=sig(torch.sum(output*target))
    return loss
metric(torch.tensor(np.ones([3,1,128,128])),torch.tensor(np.ones([3,1,128,128])))
