# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt


def match_point(point, real_idx):
    """
    return whether a point is in real_idx
    """
    x_, y_ = point
    for i in range(len(real_idx[0])):
        dist = abs(real_idx[0][i] - x_) + abs(real_idx[1][i] - y_)  # Manhattan distance
        if dist == 0:
            return True
    return False


def iou(real_data, pred_data):
    """ 
    real and pred are 2D numpy arrays with same dimension
    Measure the IoU of the two arrays
    """

    real_idx = np.nonzero(real_data)
    pred_idx = np.nonzero(pred_data)
    total = 0
    overlap = 0
    matched_point_list = set()

    if len(real_idx[0]) == 0 or len(pred_idx[0]) == 0:
        return 0

    for i in range(len(pred_idx[0])):
        point = (pred_idx[0][i], pred_idx[1][i])
        if match_point(point, real_idx):
            matched_point_list.add(point)
            total += 1
            overlap += 1
        else:
            total += 1

    real_idx_converted = set()
    for i in range(len(real_idx[0])):
        real_idx_converted.add((real_idx[0][i], real_idx[1][i]))

    if real_idx_converted.difference(matched_point_list) is not None:
        total += len(real_idx_converted.difference(matched_point_list))

    iou_result = overlap / total

    """
    plt.imshow(real_data)
    plt.title('real')
    plt.show()
    plt.imshow(pred_data)
    plt.title('pred')
    plt.show()
    print('iou:', iou_result)
    """

    return iou_result


if __name__ == "__main__":  # testing
    real = np.random.randint(2, size=36)
    real = np.reshape(real, (6, 6))
    pred = np.random.randint(2, size=36)
    pred = np.reshape(pred, (6, 6))

    real = np.array([
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0]])

    pred1 = np.array([
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]])

    pred2 = np.array([
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 0]])

    pred3 = np.array([
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0],
        [1, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0]])

    iou(real, pred1)
    iou(real, pred2)
    iou(real, pred3)
