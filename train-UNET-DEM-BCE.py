# %%
import os
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.multiprocessing

from torch.utils.tensorboard import SummaryWriter
import PIL
import sklearn.metrics as metrics

import data_loader.preprocessing as preprocessing
import model.UNet as UNet
# import distance

NAME = "UNET-DEM-BCE"
CH = 1
cutoff = 0.2

if not os.path.exists(os.path.join("checkpoints", NAME)):
    os.mkdir(os.path.join("checkpoints", NAME))

# %%
dataset = preprocessing.DEMData()
dataloader_train, dataloader_test = preprocessing.get_data_loaders(dataset)

# %%
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")  # use GPU if available
torch.backends.cudnn.benchmark = True

# %%
model = UNet.Model(CH)
if torch.cuda.device_count() > 1:
    model = nn.DataParallel(model)
model = model.to(device)
# model = nn.DataParallel(model, device_ids=[0, 1, 2, 3])

# %%
optimizer = torch.optim.Adam(model.parameters(), lr=1.0e-4)
criterion = nn.BCELoss()

# %%
writer = SummaryWriter(os.path.join("./runs/", NAME), flush_secs=10)
# writer.add_graph(model, torch.rand(1, CH, 128, 128))

# %%
global_step = 0
print("start train")
for epoch in range(40):
    print("epoch {}/{}".format(epoch, 40))
    for i, (x, label) in enumerate(dataloader_train):
        model.train()

        x = x.to(device)
        label = label.to(device)

        x = x.narrow(1, 5, 1)

        pred = model(x).double()

        optimizer.zero_grad()
        loss = criterion(pred, label)
        loss.backward()
        optimizer.step()

        global_step += 1
        if (i + 1) % 128 == 0:
            print('batch {}, loss {}'.format(i + 1, loss))
            model.eval()
            with torch.no_grad():
                tloss = 0.
                for num, (x, label) in enumerate(dataloader_test):
                    last_label = label

                    x = x.to(device)
                    label = label.to(device)

                    x = x.narrow(1, 5, 1)
                    pred = model(x)
                    pred = pred.double()
                    last_pred = pred
                    tloss += criterion(pred, label).cpu()

            vloss = tloss / (num + 1)
            print('validation Loss: {}'.format(vloss))

            c_matrix = metrics.confusion_matrix(last_label.view(-1).cpu(), last_pred.view(-1).cpu() > cutoff)
            print(c_matrix)  # --------------------------------------------------------

            tn = c_matrix[0][0]
            fp = c_matrix[0][1]
            fn = c_matrix[1][0]
            tp = c_matrix[1][1]

            precision = tp / (tp + fp)
            recall = tp / (tp + fn)
            f1_score = 2 * precision * recall / (precision + recall)

            print("precision: {}".format(precision))
            print("recall: {}".format(recall))
            print("f1 score: {}".format(f1_score))

            writer.add_scalar("train_loss", loss, global_step=global_step)
            writer.add_scalar("test_loss", vloss, global_step=global_step)
            writer.add_scalars("loss", {'train_loss': loss, 'test_loss': vloss}, global_step=global_step)
            writer.add_scalar("precision", precision, global_step=global_step)
            writer.add_scalar("recall", recall, global_step=global_step)
            writer.add_scalar("f1_score", f1_score, global_step=global_step)
    last_pred = last_pred.cpu()
    image_set = torch.cat((last_pred.unsqueeze(0), last_label.unsqueeze(0))).transpose(0, 1).contiguous().view(-1, CH, 128, 128).cpu()
    writer.add_images("test_images", image_set, global_step=global_step)
    writer.add_pr_curve("pr_curve", last_label.view(-1).cpu(), last_pred.view(-1).cpu(), global_step=global_step)

    c_matrix = np.zeros((2, 2))
    model.eval()
    with torch.no_grad():
        for num, (x, label) in enumerate(dataloader_test):
            last_label = label

            x = x.to(device)
            label = label.to(device)

            x = x.narrow(1, 5, 1)
            pred = model(x)
            pred = pred.double()

            # c_matrix += metrics.confusion_matrix((label.cpu() > 0).numpy(), (pred.cpu() > cutoff).numpy())
            c_matrix += metrics.confusion_matrix(label.view(-1).cpu(), pred.view(-1).cpu() > cutoff)

    tn = c_matrix[0][0]
    fp = c_matrix[0][1]
    fn = c_matrix[1][0]
    tp = c_matrix[1][1]

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1_score = 2 * precision * recall / (precision + recall)

    print("epoch precision: {}".format(precision))
    print("epoch recall: {}".format(recall))
    print("epoch f1 score: {}".format(f1_score))

    writer.add_scalar("full_precision", precision, global_step=global_step)
    writer.add_scalar("full_recall", recall, global_step=global_step)
    writer.add_scalar("full_f1_score", f1_score, global_step=global_step)

    if (epoch + 1) % 10 == 0:
        print("saving")
        torch.save(model.state_dict(), os.path.join("checkpoints", NAME, "checkpoint{}.pt".format(epoch + 1)))
        torch.save(optimizer.state_dict(), os.path.join("checkpoints", NAME, "_optimizer.opt"))
        print("saved")

writer.close()
