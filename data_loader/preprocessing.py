import numpy as np
import torch
import os
import pickle

from torch.utils.data import Dataset
from torch.utils.data.sampler import SubsetRandomSampler


# %%
# def save_subset_paths(path="/home/hackathon/dem/hackII"):
def save_subset_paths(path="C:/Users/Darwin/Desktop/hackII/"):
    label_path = os.path.join(path, "all_masks_5m6b")
    files_list = []
    for file in os.listdir(label_path):
        if os.path.isfile(os.path.join(label_path, file)):
            if np.sum(np.load(os.path.join(label_path, file))) > 0:
                files_list.append(file)
    files_list.sort()

    with open("files_list.pkl", "wb") as file:
        pickle.dump(files_list, file, -1)


# %%
# def read_data(path="/home/hackathon/dem/hackII"):
def data_dirs(path="C:\\Users\\Darwin\\Desktop\\hackII", subset_load=True):
    if os.path.exists("/home/hackathon/dem/hackII"):
        path = "/home/hackathon/dem/hackII"
    image_path = os.path.join(path, "all_frames_5m6b")
    label_path = os.path.join(path, "all_masks_5m6b")
    if subset_load:
        with open("files_list.pkl", "rb") as file:
            files_list = pickle.load(file)
    else:
        files_list = [file for file in os.listdir(image_path) if os.path.isfile(os.path.join(image_path, file))]
        files_list.sort()

    image_dirs = [os.path.join(image_path, file) for file in files_list]
    label_dirs = [os.path.join(label_path, file) for file in files_list]

    return zip(image_dirs,label_dirs)


# %%
class DEMData(Dataset):
    def __init__(self):
        dirs = data_dirs()

        self.images = []
        self.labels = []

        for image_dir, label_dir in dirs:
            image = np.load(image_dir)
            label = np.load(label_dir)

            if np.sum(label) == 0:
                continue

            dem = image[:, :, 5]
            dem[dem == 0] = np.nan
            dem = (dem - 800) / 50
            dem[np.isnan(dem)] = 0
            image[:, :, 5] = dem

            self.images.append(image.transpose((2,0,1)))
            self.labels.append(label)

    def __len__(self):
        return 8 * len(self.images)

    def __getitem__(self, idx):
        image = torch.from_numpy(self.images[int(idx / 8)])
        label = torch.from_numpy(self.labels[int(idx / 8)])
        if idx % 2 < 1:
            image = torch.flip(image, [1])
            label = torch.flip(label, [0])
        if idx % 4 < 2:
            image = torch.flip(image, [2])
            label = torch.flip(label, [1])
        if idx % 8 < 4:
            image = torch.transpose(image, 1, 2)
            label = torch.transpose(label, 0, 1)
        return image, label


# %%
def get_data_loaders(dataset, train_batch_size=32, test_batch_size=32):
    indices = np.asarray(range(int(len(dataset) / 8)))
    np.random.seed(42)
    np.random.shuffle(indices)
    indices = np.stack([indices * 8 + i for i in range(8)], axis=-1).reshape(-1)
    split = int(np.floor(0.8 * len(dataset) / 8) * 8)
    train_indices, test_indices = indices[:split], indices[split:]

    sampler_train = SubsetRandomSampler(train_indices)
    sampler_test = SubsetRandomSampler(test_indices)

    dataloader_train = torch.utils.data.DataLoader(dataset, batch_size=train_batch_size, sampler=sampler_train)
    dataloader_test = torch.utils.data.DataLoader(dataset, batch_size=test_batch_size, sampler=sampler_test)

    return dataloader_train, dataloader_test